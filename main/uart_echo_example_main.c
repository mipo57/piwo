#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/uart.h"
#include "driver/adc.h"
#include "driver/gpio.h"
#include "config.h"
#include "lcd_driver.h"

float winder_level = 0;

void init_heater_driver() {
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = BIT(HEATER_PIN);
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);

    gpio_set_level(HEATER_PIN, 0);
}

inline void onewire_setdir_out() {
    gpio_set_direction(TERM_PIN, GPIO_MODE_OUTPUT);
}

void onewire_setdir_in() {
    gpio_set_direction(TERM_PIN, GPIO_MODE_INPUT);
}

bool onewire_ResetPulse() {
    bool ucPresenceImpulse;

    onewire_setdir_out();
    gpio_set_level(TERM_PIN, 0);
    ets_delay_us(500);
    onewire_setdir_in();
    ets_delay_us(65);

    ucPresenceImpulse = !gpio_get_level(TERM_PIN);
    return ucPresenceImpulse;
}

void onewire_SendBit(unsigned char bit) {
    onewire_setdir_out();
    ets_delay_us(1);
    if (bit)
        onewire_setdir_in();

    ets_delay_us(70);

    if (!bit)
        onewire_setdir_in();
}

unsigned char onewire_ReadBit() {
    unsigned char bit;
    
    onewire_setdir_out();
    gpio_set_level(TERM_PIN, 0);
    ets_delay_us(2);
    onewire_setdir_in();
    ets_delay_us(15);

    return gpio_get_level(TERM_PIN);
}

void onewire_sendbyte(char data) {
    for (int i = 0; i < 8; i++) {
        onewire_SendBit(data & 1);
        data = data >> 1;
    }

    ets_delay_us(100);
}

char onewire_readbyte() {
    char output = 0x00;
    char bit = 0;

    for (int i = 0; i < 8; i++) {
        bit = onewire_ReadBit();
        output |= (1 << i) * bit;
        ets_delay_us(15);
    }

    return output;
}

float read_temp() {
    bool has_response = onewire_ResetPulse();

    if (!has_response)
        return 0;

    onewire_sendbyte(0xCC);
    onewire_sendbyte(0x44);
    vTaskDelay(pdMS_TO_TICKS(750));
    onewire_ResetPulse();
    onewire_sendbyte(0xCC);
    onewire_sendbyte(0xBE);

    char low_byte = onewire_readbyte();
    char high_byte = onewire_readbyte();
    onewire_ResetPulse();

    return (float)(low_byte + (high_byte<<8))/16;
}

void initialize_uart_communication() {
    uart_config_t uart_config = {
        .baud_rate = BAUD_RATE,
        .data_bits = UART_DATA_8_BITS,
        .parity    = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };
    uart_param_config(UART_NUM_0, &uart_config);
    uart_driver_install(UART_NUM_0, UART_BUFFER_SIZE*2, 0, 0, NULL);

}

static void heater_task() {
    init_heater_driver();
    int counter = 0;

    while (1) {
        vTaskDelay(pdMS_TO_TICKS( 10 ) );
        uint16_t adc_level = adc_read();
        uint level = adc_level / 10.2;

        if (counter < level)
            gpio_set_level(HEATER_PIN, 1);
        else
            gpio_set_level(HEATER_PIN, 0);

        counter = (counter + 1) % 101;
    }
}

static void winder_task() {
    while (1) {
        vTaskDelay(pdMS_TO_TICKS( 10 ) );
        uint16_t adc_level = adc_read();
        winder_level = adc_level / 1023.0;
    }
}

static void uart_task()
{
    initialize_uart_communication();

    // Configure a temporary buffer for the incoming data
    uint8_t *data = (uint8_t *) malloc(UART_BUFFER_SIZE);

    lcd_i2c_init();

    // Tryb 4-bitowy
    lcd_write_pins(0b00100100);
    lcd_write_pins(0b00000100);

    lcd_write_pins(0b00100100);
    lcd_write_pins(0b00000100);

    lcd_write_pins(0b00101000);
    lcd_write_pins(0b00001000);

    // Clear
    lcd_write_pins(0b00100000);
    lcd_write_pins(0b00000000);
    lcd_write_pins(0b00101000);
    lcd_write_pins(0b00001000);

    // Cursor
    lcd_write_pins(0b00100000);
    lcd_write_pins(0b00000000);
    lcd_write_pins(0b00101111);
    lcd_write_pins(0b00001111);

    char a = 'a';
    uint8_t val1 = 0b10000000 | (a & 0xf0) >> 4;
    uint8_t val2 = 0b10000000 | (a & 0x0f);


    lcd_write_pins(0b01001010);
    lcd_write_pins(0b01101010);
    lcd_write_pins(0b01001010);


    lcd_write_pins(0b01001000);
    lcd_write_pins(0b01101000);
    lcd_write_pins(0b01001000);


    
    //lcd_write_pins(val2 | 0b00100000);
    //lcd_write_pins(val2);

    //lcd_init();
    //lcd_write_hello();
    // RS RW E NC D4 D5 D6 D7
    //lcd_write_pins(0b00000000);

    while (1) {
        float temp = 0;//read_temp();
        char buffer[50] = {0};
        gcvt(temp, 5, buffer);
        ///sprintf((const char*)buffer, "Lubie placki: %f\n", temp);
        uart_write_bytes(UART_NUM_0, buffer, strlen(buffer));
        uart_write_bytes(UART_NUM_0, "\n", strlen("\n"));
        vTaskDelay(pdMS_TO_TICKS( 1000 ) );
    }
}

void app_main()
{
    //xTaskCreate(heater_task, "heater_task", 64, NULL, 10, (TaskHandle_t*)NULL);
    //xTaskCreate(winder_task, "winder_task", 64, NULL, 5, (TaskHandle_t*)NULL);
    xTaskCreate(uart_task, "uart_task", 1024, NULL, 5, (TaskHandle_t*)NULL);
}
