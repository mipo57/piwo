#if !defined(CONFIG_H)
#define CONFIG_H

/**********
 *  UART  *
 **********/
#define UART_BUFFER_SIZE (1024)
#define BAUD_RATE 74880 

#define HEATER_PIN (4)
#define TERM_PIN (5)

#endif // CONFIG_H
