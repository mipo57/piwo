#include "lcd_driver.h"

void lcd_i2c_init()
{
    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = 1;
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = 1;
    i2c_driver_install(i2c_master_port, conf.mode);
    i2c_param_config(i2c_master_port, &conf);
}

void lcd_write_pins(uint8_t pins)
{
    int ret;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, LCD_I2C_ADDRESS << 1 | 0, 1);
    i2c_master_write_byte(cmd, pins, 1);
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
}

void lcd_init() {
   vTaskDelay(pdMS_TO_TICKS( 15 ) );;             // wait 15msec
   lcd_write_pins(0b00110100); // D7=0, D6=0, D5=1, D4=1, RS,RW=0 EN=1
   lcd_write_pins(0b00110000); // D7=0, D6=0, D5=1, D4=1, RS,RW=0 EN=0
   ets_delay_us(4100);              // wait 4.1msec
   lcd_write_pins(0b00110100); // 
   lcd_write_pins(0b00110000); // same
   ets_delay_us(100);               // wait 100usec
   lcd_write_pins(0b00110100); //
   lcd_write_pins(0b00110000); // 8-bit mode init complete
   ets_delay_us(4100);              // wait 4.1msec
   lcd_write_pins(0b00100100); //
   lcd_write_pins(0b00100000); // switched now to 4-bit mode

   /* -------------------------------------------------------------------- *
    * 4-bit mode initialization complete. Now configuring the function set *
    * -------------------------------------------------------------------- */
   ets_delay_us(40);                // wait 40usec
   lcd_write_pins(0b00100100); //
   lcd_write_pins(0b00100000); // keep 4-bit mode
   lcd_write_pins(0b10000100); //
   lcd_write_pins(0b10000000); // D3=2lines, D2=char5x8

   /* -------------------------------------------------------------------- *
    * Next turn display off                                                *
    * -------------------------------------------------------------------- */
   ets_delay_us(40);                // wait 40usec
   lcd_write_pins(0b00000100); //
   lcd_write_pins(0b00000000); // D7-D4=0
   lcd_write_pins(0b10000100); //
   lcd_write_pins(0b10000000); // D3=1 D2=display_off, D1=cursor_off, D0=cursor_blink

   /* -------------------------------------------------------------------- *
    * Display clear, cursor home                                           *
    * -------------------------------------------------------------------- */
   ets_delay_us(40);                // wait 40usec
   lcd_write_pins(0b00000100); //
   lcd_write_pins(0b00000000); // D7-D4=0
   lcd_write_pins(0b00010100); //
   lcd_write_pins(0b00010000); // D0=display_clear

   /* -------------------------------------------------------------------- *
    * Set cursor direction                                                 *
    * -------------------------------------------------------------------- */
   ets_delay_us(40);                // wait 40usec
   lcd_write_pins(0b00000100); //
   lcd_write_pins(0b00000000); // D7-D4=0
   lcd_write_pins(0b01100100); //
   lcd_write_pins(0b01100000); // print left to right

   /* -------------------------------------------------------------------- *
    * Turn on the display                                                  *
    * -------------------------------------------------------------------- */
   ets_delay_us(40);                // wait 40usec
   lcd_write_pins(0b00000100); //
   lcd_write_pins(0b00000000); // D7-D4=0
   lcd_write_pins(0b11100100); //
   lcd_write_pins(0b11100000); // D3=1 D2=display_on, D1=cursor_on, D0=cursor_blink
}

void lcd_write_hello() {
   lcd_write_pins(0b01001101); //
   lcd_write_pins(0b01001001); // send 0100=4
   lcd_write_pins(0b10001101); //
   lcd_write_pins(0b10001001); // send 1000=8 = 0x48 ='H'

   lcd_write_pins(0b01001101); //
   lcd_write_pins(0b01001001); // send 0100=4
   lcd_write_pins(0b01011101); // 
   lcd_write_pins(0b01011001); // send 0101=1 = 0x41 ='E'

   lcd_write_pins(0b01001101); //
   lcd_write_pins(0b01001001); // send 0100=4
   lcd_write_pins(0b11001101); //
   lcd_write_pins(0b11001001); // send 1100=12 = 0x4D ='L'

   lcd_write_pins(0b01001101); //
   lcd_write_pins(0b01001001); // send 0100=4
   lcd_write_pins(0b11001101); //
   lcd_write_pins(0b11001001); // send 1100=12 = 0x4D ='L'

   lcd_write_pins(0b01001101); //
   lcd_write_pins(0b01001001); // send 0100=4
   lcd_write_pins(0b11111101); //
   lcd_write_pins(0b11111001); // send 1111=15 = 0x4F ='O'
}

//-------------------------------------------------------------------------------------------------
//
// EOF HD44780.c
//
//-------------------------------------------------------------------------------------------------